import { Component, OnInit, createPlatformFactory } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  
  cartItems: any;
  cartProducts: any;
  total: number;

  constructor(private service: EmpService) {
    this.total = 0;
    this.cartItems = service.getCartItems();

    this.cartItems.forEach((element: any) => {
      this.total += element.price;
    });
  }
  
  ngOnInit() {    
    //this.cartProducts = localStorage.getItem("cartItems");
    //this.cartItems = JSON.parse(this.cartProducts);
  }

}
